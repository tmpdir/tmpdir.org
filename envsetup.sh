#!/bin/sh

export GOOGLE_APPLICATION_CREDENTIALS=~/podcast/gcloud-tmpdir-upload-key.json

tmp_start_episode() {
	MP3FILE=$1
	EPISODE=$(basename "$MP3FILE" .mp3)
	echo $EPISODE
	if [ -z "$EPISODE" ]; then
		echo "Usage: tmp_start_issue <path to mp3 file>"
		return 1
	fi

	if [ $(expr length "$EPISODE") != "3" ]; then
		echo "Issue must be 3 digits"
		return 1
	fi

	git checkout master
	git pull
	git checkout -b "episode-$EPISODE"

	go run main.go "$MP3FILE"

	git add .
	git commit -m "Start episode $EPISODE"
	git push -u origin HEAD
}

tmp_publish() {
	(cd /scratch/bec/ops &&
		ansible-playbook -i production all.yml --limit tmpdir --tags tmpdir.org)
}

tmp_view() {
	hugo serve
}

# not used, instead we now use main.go
# below is just for testing/debugging
tmp_mp3_duration() {
	FILE=$1
	ffmpeg -i $FILE 2>&1 | grep Duration
}
