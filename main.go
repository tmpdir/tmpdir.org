package main

import (
	"context"
	"fmt"
	"html/template"
	"io"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"cloud.google.com/go/storage"
	"github.com/hajimehoshi/go-mp3"
)

type episodeData struct {
	FileNumber string
	Number     int
	Date       string
	Seconds    int
	FileSize   int
}

func gcloudUpload(path, bucketName string) error {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatalf("Failed to create GCS client: %v", err)
	}

	bucket := client.Bucket(bucketName)

	file, err := os.Open(path)
	if err != nil {
		log.Fatalf("Failed to open local file: %v", err)
	}
	defer file.Close()

	object := bucket.Object(filepath.Base(path))
	writer := object.NewWriter(ctx)
	if _, err = io.Copy(writer, file); err != nil {
		return fmt.Errorf("Failed to upload file to GCS: %v", err)
	}
	if err := writer.Close(); err != nil {
		return fmt.Errorf("Failed to close GCS writer: %v", err)
	}

	return err
}

func main() {
	ed := episodeData{}
	ed.Date = time.Now().Format(time.RFC3339)

	if len(os.Args) < 2 {
		fmt.Println("Usage: go run main.go <filename>")
		return
	}

	path := os.Args[1]

	filename := filepath.Base(path)
	extension := filepath.Ext(filename)

	ed.FileNumber = filename[:len(filename)-len(extension)]

	var err error
	ed.Number, err = strconv.Atoi(ed.FileNumber)
	if err != nil {
		log.Fatal("Error converting file name to number: ", err)
	}

	info, err := os.Stat(path)
	if err != nil {
		log.Fatalf("Error get file stats: %v\n", err)
	}

	ed.FileSize = int(info.Size())

	f, err := os.Open(path)
	if err != nil {
		log.Fatalf("Error opening file: %v\n", err)
	}
	defer f.Close()

	d, err := mp3.NewDecoder(f)
	if err != nil {
		log.Fatalf("Error creating decoder: %v\n", err)
		return
	}

	bytes := d.Length()
	// 4 bytes/sample
	samples := bytes / 4
	ed.Seconds = int(samples / int64(d.SampleRate()))
	duration := time.Duration(ed.Seconds) * time.Second
	fmt.Printf("Duration: %v\n", duration)

	fmt.Printf("MP3 episode data: %+v\n", ed)

	// populate template
	destPath := filepath.Join("./", "content", "episode", ed.FileNumber+".md")
	wf, err := os.Create(destPath)
	if err != nil {
		log.Fatal("Error opening write file")
	}
	defer wf.Close()

	tbytes, err := os.ReadFile("episode.md")
	if err != nil {
		log.Fatal("Error reading episode template: ", err)
	}

	t := template.New("episode")
	t, err = t.Parse(string(tbytes))
	if err != nil {
		log.Fatal("Error parsing template: ", err)
	}

	err = t.Execute(wf, ed)
	if err != nil {
		log.Fatal("Error executing template: ", err)
	}

	log.Println("Uploading ...")
	err = gcloudUpload(path, "tmpdir-podcast")
	if err != nil {
		log.Fatal("Error uploading podcast: ", err)
	}

	log.Println("File uploaded")
}
