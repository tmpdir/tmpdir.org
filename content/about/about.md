+++
title = "about us"
description = "about this site"
Date = 2020-04-10T10:13:16-04:00
+++

We welcome feedback and suggestions. To join our community and stay current on
new developments, please subscribe to our
[newsletter](https://tmpdir.ck.page/196d1fb480) and join our
[community Discourse site](https://community.tmpdir.org/).

You can listen to this podcast here, any player that supports RSS feeds, or on
the following platforms:

- [Apple](https://podcasts.apple.com/us/podcast/the-tmpdir-podcast/id1527275871)
- [Spotify](https://open.spotify.com/show/5zUgTtuQVwnKmVenRawX56)
