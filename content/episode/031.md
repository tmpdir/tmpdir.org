+++
title = "Drew Moseley from Toradex"
Description = "Discussion with Drew about Embedded Linux, technologies, and challenges."
Date = 2024-05-18T14:36:58-04:00
PublishDate = 2024-05-18T14:36:58-04:00
podcast_file = "031.mp3"
# 24:21
podcast_duration = "2880"  # the length in seconds
podcast_bytes = "44207080" # the length of the episode in bytes
episode_image = "img/episode/default.jpg"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
episode = "31"
images = ["img/episode/default-social.jpg"]
hosts = ["cliff", "khem"] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
# media_override # if you want to use a specific URL for the audio file
# truncate = ""
+++

Available on your favorite podcast [platform](https://tmpdir.org/about/).

In this episode, we had a great discussion with
[Drew](https://www.linkedin.com/in/drewmoseley/) about:

- Drew's career -- many interesting companies
- Toradex
  - manufacturers SOMs (System-on-Module)
  - started out providing WinCE solutions
- Torizon
  - opinionated stack
  - fully integration solution
  - container runtime -- allows developers to deploy applications without
    knowing much about Linux
  - customize OS without doing Yocto builds
- Visual Studio Code plugin
  - simplifies management of Docker images, containers, cross compilation,
    remote debugging, etc.
- Torizon device side software is OSS
- SW update system
  - many options: Mendor, RAUC, SWUpdate, OSTree
  - Toradex uses OSTree
    - OSTree is file based for delta updates, where RAUC and SWUpdate are block
      based.
    - single partition
    - Git-like store where changes can be rolled back, etc.
    - read-only rootfs
  - Uptane is provides protection against attackers
- Greenboot -- used to verify system is functioning properly
- Toradex's upstream first policy
- Challenges in developing with Embedded Linux
  - drinking from the OSS firehose
  - understanding all the components
  - realistic expectations about how long this stuff takes to develop
  - regulations
- The future
  - immutable distros

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).
