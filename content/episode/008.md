+++
title = "Using Elm for frontend development"
Description = "In this episode we discuss our experience using the Elm language for web application development."
Date = 2021-02-12T10:34:15-04:00
PublishDate = 2021-02-12T10:34:15-04:00 
podcast_file = "008.mp3"
podcast_duration = "1859"
podcast_bytes = "35016213" # the length of the episode in bytes
episode_image = "img/episode/default.jpg"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
episode = "8"
images = ["img/episode/default-social.jpg"]
hosts = ["cliff", "khem"] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
# media_override # if you want to use a specific URL for the audio file
# truncate = ""
+++

In this episode we discus:

- does Elm really live up to the expectation of no run time exceptions?
- Elm tooling
- elm-ui
- how Elm interacts with Javascript
- how Elm compares to other front end technologies. As a follow up we
  [documented bundle size for various frameworks](https://community.tmpdir.org/t/comparison-of-asset-sizes-for-various-front-end-technologies/213).
- why Elm is a really good option for those of us who are not front end experts.

Available on your favorite podcast [platform](https://tmpdir.org/about/).

Bonus:
[What are your favorite things about Elm?](https://discourse.elm-lang.org/t/what-are-your-favorite-things-about-elm/6947)

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).
