+++
title = "Product vs. Technology"
Description = "In this episode, we discuss why and how to differentiate product and technology."
Date = 2025-02-28T16:55:15-05:00
PublishDate = 2025-02-28T16:55:15-05:00
podcast_file = "041.mp3"
# 24:21
podcast_duration = "532"  # the length in seconds
podcast_bytes = "8573365" # the length of the episode in bytes
episode_image = "img/episode/default.jpg"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
episode = "41"
images = ["img/episode/default-social.jpg"]
hosts = ["cliff", "khem"] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
# media_override # if you want to use a specific URL for the audio file
# truncate = ""
+++

Available on your favorite podcast [platform](https://tmpdir.org/about/).

In this episode, we discuss the difference between a product and technology. A
couple related blog posts from Khem on this subject:

- [Product and platform/technology](https://daily.himvis.com/0066-a-case-for-opensource-in-product-and-platform-technology/)
- [Opensource in context of product and platform/technology - Example](https://daily.himvis.com/0068-opensource-in-context-of-product-and-platform-technology-example/)

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).
