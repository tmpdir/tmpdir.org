+++
title = "Discussion with Andrew Wafaa"
Description = "In this episode we discuss many aspects of Open Source and learn from Andrew's experience."
Date = 2021-04-15T10:34:15-04:00
PublishDate = 2021-04-15T10:34:15-04:00 
podcast_file = "011.mp3"
# 45:05
podcast_duration = "3195"
podcast_bytes = "50284679" # the length of the episode in bytes
episode_image = "img/episode/default.jpg"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
episode = "11"
images = ["img/episode/default-social.jpg"]
hosts = ["cliff", "khem"] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
# media_override # if you want to use a specific URL for the audio file
# truncate = ""
+++

Our guest on this episode is
[Andrew Wafaa](https://www.linkedin.com/in/andrewwafaa/). We discussed many
aspects of open source and technology. Near the end of the episode, Andrew
shares some advice for people who want to learn about technology.

Show notes:

- Andrew has been involved in OSS for 2 decades
- likes packaged software -- started by packaging software for SUSE for his own
  use
- too many people are concerned about what others will think of their work
- you keep doing OSS for yourself.
- successful community requires a safe, open, collaborative environment
- accept the fact you can't please everyone. Once you try to please everyone,
  you please no one.
- marketing is the Achilles heel of many projects
- events are an easy way to market projects
- BSD
  - very nice community, and BSP projects seem to be thriving
  - collaboration between commercial entities is better than Linux -- which may
    seem seem a bit surprising considering BSD is licensed with a permissive
    license
  - used a lot in education -- great learning platform
- tips
  - read the documentation
  - build up a portfolio (Github, etc)
  - try it all out
  - don't be afraid to ask
  - have fun
  - operating systems/distros are a good way to get exposed to a lot of
    different technologies -- languages, compilers, projects, etc.

Available on your favorite podcast [platform](https://tmpdir.org/about/).

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).
