+++
title = "The Yoe Updater"
Description = "In this episode, we discuss the Yoe updater and why it exists."
Date = 2021-01-22T10:34:15-04:00
PublishDate = 2021-01-22T10:34:15-04:00 
podcast_file = "007.mp3"
podcast_duration = "2778"
podcast_bytes = "46777604" # the length of the episode in bytes
episode_image = "img/episode/default.jpg"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
episode = "7"
images = ["img/episode/default-social.jpg"]
hosts = ["cliff", "khem"] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
# media_override # if you want to use a specific URL for the audio file
# truncate = ""
+++

In this episode we discus:

- review of various Linux update technologies
- discussion of experiments using swupdate
- description of the Yoe updater
- design ideas behind the Yoe updater
- recent work done on the Yoe updater to make it more generic
- Yoe handles installation, system initialization as well as update - this is
  very critical
- designed to support products with long life cycles where things may change
  (partition layout, etc.)
- how to add Yoe updater support to a new platform
- what pieces of the system does the Yoe updater touch
- how the Yoe updater handles rootfs corruption
- why system initialization is important for reliable systems
- why system initialization make sense in an initramfs
- keep it simple is really the mantra
- how the Yoe updater helps developers streamline their testing process by
  removing friction
- future plans of managing updates using Simple IoT

See also the
[documentation](https://github.com/YoeDistro/yoe-distro/blob/master/docs/updater.md)
and
[implementation](https://github.com/YoeDistro/yoe-distro/tree/master/sources/meta-yoe/recipes-support/updater).

Available on your favorite podcast [platform](https://tmpdir.org/about/).

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).
