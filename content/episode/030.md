+++
title = "Embedded Open Source Summit Recap"
Description = "Khem attended EOSS and shares some of his thoughts about the event."
Date = 2024-05-16T21:11:56-04:00
PublishDate = 2024-05-16T21:11:56-04:00
podcast_file = "030.mp3"
# 24:21
podcast_duration = "1357"  # the length in seconds
podcast_bytes = "20353848" # the length of the episode in bytes
episode_image = "img/episode/default.jpg"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
episode = "30"
images = ["img/episode/default-social.jpg"]
hosts = ["cliff", "khem"] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
# media_override # if you want to use a specific URL for the audio file
# truncate = ""
+++

Available on your favorite podcast [platform](https://tmpdir.org/about/).

The Embedded Open Source Summit is a Linux Foundation event that covers embedded
computing systems (mainly Linux and Zephyr). Listen to learn industry trends
that may affect your product development efforts.

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).
