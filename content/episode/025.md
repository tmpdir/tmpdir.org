+++
title = "What is new in the Yoe Distribution"
Description = "A discussion on some of the new platforms and features in the Yoe Distribution."
Date = 2023-11-27T09:59:47-05:00
PublishDate = 2023-11-27T09:59:47-05:00
podcast_file = "025.mp3"
# 24:21
podcast_duration = "1662"  # the length in seconds
podcast_bytes = "26024348" # the length of the episode in bytes
episode_image = "img/episode/default.jpg"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
episode = "25"
images = ["img/episode/default-social.jpg"]
hosts = ["cliff", "khem"] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
# media_override # if you want to use a specific URL for the audio file
# truncate = ""
+++

Available on your favorite podcast [platform](https://tmpdir.org/about/).

In this episode, we discuss the latest features in the Yoe Distribution that can
help you build products more efficiently using Yocto.

- New platforms:
  - Nvidia Jetson Orin Nano
  - VisionFive2 RISCV64 SBC
- Yoe Projects - a better way to configure your project
- Qt 6.6
- switch to LLD linker
- ptests can not be run inside Docker

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).
