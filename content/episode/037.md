+++
title = "Recent experiences building a connected device with Zephyr"
Description = "Discussion on what is like to build using Zephyr."
Date = 2024-10-18T17:36:07-04:00
PublishDate = 2024-10-18T17:36:07-04:00
podcast_file = "037.mp3"
# 24:21
podcast_duration = "1688"  # the length in seconds
podcast_bytes = "27223063" # the length of the episode in bytes
episode_image = "img/episode/default.jpg"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
episode = "37"
images = ["img/episode/default-social.jpg"]
hosts = ["cliff", "khem"] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
# media_override # if you want to use a specific URL for the audio file
# truncate = ""
+++

Available on your favorite podcast [platform](https://tmpdir.org/about/).

In this episode, we discuss our recent experiences building a product using
Zephyr, and how Zephyr has helped us move fast and quickly integrate advanced
features like a HTTP server, non-voltile store in flash, device support, etc. We
also discuss how to set up a project in Zephyr and have built a project template
that demostrates how to use the Zephyr T2 workspace setup. This template also
includes common things projects need like a web application example.

[Zephyr SimpleIoT](https://github.com/simpleiot/zephyr-siot)

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).
