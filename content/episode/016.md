+++
title = "Why 64-bit?"
Description = "In this episode, we discuss why you should consider 64-bit for Embedded Linux project."
Date = 2022-12-09T10:34:15-04:00
PublishDate = 2022-12-09T10:34:15-04:00 
podcast_file = "016.mp3"
# 24:21
podcast_duration = "1687"  # the length in seconds
podcast_bytes = "28180733" # the length of the episode in bytes
episode_image = "img/episode/default.jpg"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
episode = "16"
images = ["img/episode/default-social.jpg"]
hosts = ["cliff", "khem"] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
# media_override # if you want to use a specific URL for the audio file
# truncate = ""
+++

Available on your favorite podcast [platform](https://tmpdir.org/about/).

In this episode, we discuss why you should consider a 64-bit processor for your
embedded Linux project.

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).
