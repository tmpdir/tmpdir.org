+++
title = "New Tools in 2024"
Description = "Discussion of new tools we found in 2024"
Date = 2025-02-14T16:50:41-05:00
PublishDate = 2025-02-14T16:50:41-05:00
podcast_file = "040.mp3"
# 24:21
podcast_duration = "1221"  # the length in seconds
podcast_bytes = "20146784" # the length of the episode in bytes
episode_image = "img/episode/default.jpg"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
episode = "40"
images = ["img/episode/default-social.jpg"]
hosts = ["cliff", "khem"] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
# media_override # if you want to use a specific URL for the audio file
# truncate = ""
+++

Available on your favorite podcast [platform](https://tmpdir.org/about/).

In this episode, we discuss some of the new tools we started using (more) in
2024 that can help you become a more productive developer.

- [Zellij](https://zellij.dev/)
- [Helix](https://helix-editor.com/)
- Terminals
  - [Alacritty](https://alacritty.org/)
  - [Kitty](https://sw.kovidgoyal.net/kitty/)
  - [Ghostty](https://ghostty.org/)
- [tio](https://github.com/tio/tio)
- [fd](https://github.com/sharkdp/fd)
- [rg](https://github.com/BurntSushi/ripgrep)
- [Perplexity](https://www.perplexity.ai/)
- [Yazi](https://yazi-rs.github.io/)
- [LazyGit](https://github.com/jesseduffield/lazygit)

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).
