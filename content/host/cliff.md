+++
Date = 2020-04-10T10:47:23-04:00
title = "Cliff Brake"
Twitter = ""
Website = "http://bec-systems.com"
Type = "host"
Facebook = ""
Linkedin = ""
GitHub = "cbrake"
Thumbnail = "img/host/cliff.png"
Pinterest = ""
Instagram = ""
YouTube = ""
Twitch = ""
+++

Cliff has been developing products for a long time. See [BEC](http://bec-systems.com) and [Github](https://github.com/cbrake) for more information.
