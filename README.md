# TMPDIR Podcast website

- run development server and show draft posts: `hugo server -D`
- generate web site (outputs files in `public` directory):
  `HUGO_ENV=production hugo`
- create a new post: `hugo new posts/<post-name>.md`
