+++
title = "TODO: TITLE"
Description = "TODO: DESCRIPTION"
Date = {{ .Date }}
PublishDate = {{ .Date }}
podcast_file = "{{ .FileNumber }}.mp3"
# 24:21
podcast_duration = "{{ .Seconds }}"  # the length in seconds
podcast_bytes = "{{ .FileSize }}" # the length of the episode in bytes
episode_image = "img/episode/default.jpg"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
episode = "{{ .Number }}"
images = ["img/episode/default-social.jpg"]
hosts = ["cliff", "khem"] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
# media_override # if you want to use a specific URL for the audio file
# truncate = ""
+++

Available on your favorite podcast [platform](https://tmpdir.org/about/).

TODO: OUTLINE In this episode, we discuss ... that can help you ...

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).
